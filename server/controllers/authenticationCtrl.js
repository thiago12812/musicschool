'use strict';

const shortid  = require('shortid');
const JWT = require('jsonwebtoken');
const Config = require('config');
const requestIp = require('request-ip');
const Session = require('middleware/common/session');

class AuthenticationController {

  constructor(server) {
    this.server = server;
  }

  async createSession(model, ip){
    let session = {
      id: shortid.generate(),
      exp: new Date().getTime() + 300 * 60 * 1000 ,
      obj: {
        id: model._id, 
        login: model.login,
        contact: model.contact._id, 
        name: model.contact.name,
        isAdmin: model.isAdmin,
        tentant: model.tentant.federalRegister,
        queue: model.tentant.queue,
      },
    };

    let token = JWT.sign(session, Config.get('server.authKey'));
    Session.set(session.id, session, session.exp);

    return token;

  }

  async signin(request, h) {

    try{
      let ip = requestIp.getClientIp(request);
      let account = request.payload;
      if (account.login === undefined) {
        return h.unauthorized();
      }

      let userServices = new UserServices();
      let user = await userServices.getByLogin(account.login)
    
      if(typeof user.license =='undefined'){
        return h.badRequest('login não encontrado');
      }
      let accountServices = new AccountServices({database:user.license.federalRegister});
      let account2 = await accountServices.getByUser(user)
      if(!account2.isValidPassword(account.password)) {
        return h.unauthorized('Usúario ou senha inválidos!');
      }
      accountServices.updateLastLogin(account2._id, ip);
        let token = await this.createSession(account2, ip);
        return  h.response({'token': token}).header('Authorization', token); 
    }catch(e){
      return h.error(e)
    }
  }

  async signout(request, h) {
    var id = request.params.id;
    this.accountServices.getById(id)
      .then(h)
      .catch((e)=> h.response(e))
  }

}

module.exports = AuthenticationController;