'use strict';

const Session = require('middleware/common/session');
const check = require('check-types');

module.exports = async function(decoded, request) {
    let result = await Session.get(decoded.id)
    if(check.undefined(decoded.obj))  {
      return { isValid: false }
    }
    
    if (check.undefined(result.obj['login'])){
      return { isValid: false }
    }
    if (decoded.obj.login === result.obj.login) {
      return { isValid: true }
    } else {
      return { isValid: false }
    }
};