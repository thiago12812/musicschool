'use strict';

const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const errorHandler = require('errorhandler');
// const Hapi = require('hapi');
var cookieParser = require('cookie-parser');

require('dotenv').config();

// Configures server to run on hapi
/*
const init = async () => {
  const server = Hapi.server({
      port: 5000,
      host: 'localhost'
  });
  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();
*/

// Configures server to run on express

//Configure mongoose's promise to global promise
mongoose.promise = global.Promise;

//Configure isProduction variable
const isProduction = process.env.NODE_ENV === 'production';

//Initiate app
const app = express();
const port = process.env.PORT || 5000;

//Configure app
app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true });

const connection = mongoose.connection;
connection.once('open', () => { 
	console.log('Successful connection to database');
});

const usersRouter  = require('./routes/users');
app.use('/users', usersRouter);

const subjectsRouter  = require('./routes/subjects');
app.use('/subjects', subjectsRouter);

const classroomRouter  = require('./routes/classroom');
app.use('/classroom', classroomRouter);

app.listen(port, () => {
	console.log(`Server running on port: ${port}`);
});