'use strict';

const router = require("express").Router();
const Joi = require('joi');
const Controller = require('controllers/classroomCtrl');

let Classroom = require('../models/classroom.model');

router.route('/').get((req, res) => {
	Classroom.find()
		.then(classroom => res.json(classroom))
		.catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
  Classroom.findByIdAndDelete(req.params.id)
    .then(() => res.json('Classroom deleted.'))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) =>{ 
	const classroomName = req.body.classroomName;
	const description = req.body.description;
	const teacher = req.body.teacher;
	const students = req.body.students;


	const newClassroom = new Classroom ({ classroomName, description, teacher, students });

	newClassroom.save()
		.then(() => res.json('classroom added'))
		.catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;