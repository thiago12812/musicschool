'use strict ';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const { ObjectId } = Schema;

const classroomSchema = new Schema ({
	classroomId:{ type: ObjectId, ref: 'Classroom' },
	classroomName: { type: String, required: true },
	teachers: [{ id: { type: ObjectId, ref: 'User' } }],
	students: [{ id: { type: ObjectId, ref: 'User' } }],
	
	description: { type: String }

});

const Classroom = mongoose.model('Classroom', classroomSchema);

module.exports = Classroom;